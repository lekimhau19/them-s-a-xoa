<?php
require_once('dbhelp.php');
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>student management</title>
	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

	<!-- jQuery library -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.1/jquery.min.js"></script>

	<!-- Latest compiled JavaScript -->
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>	
</head>
<body>
	<div class="container">
		<div class="panel panel-primary">
			<div class="panel panel-heading">
				Quản lý thông tin sinh viên
			</div>
			<div class="panel panel-body">
				<table class="table table-bordered">
					<thead>
						<tr>
							<th>STT</th>
							<th>Họ Và Tên</th>
							<th>Tuổi</th>
							<th>Địa Chỉ</th>
							<th style="width: 60px;"></th>
							<th style="width: 60px;"></th>
						</tr>
					</thead>
					<tbody>
						<?php
						$sql = 'select * from student';
						$studentList = executeResult($sql);
						$index=1;
						foreach ($studentList as $std){
						echo '<tr>
										<td>'.($index++).'</td>
										<td>'.$std['Fullname'].'</td>
										<td>'.$std['Age'].'</td>
										<td>'.$std['Address'].'</td>
										<td><button class="btn btn-info">Edit</button></td>
										<td><button class="btn btn-danger">Delete</button></td>
									</tr>';

						}
						?>
					</tbody>
				</table>
		</div>
	</div>
				<a href="input.php"><button class="btn btn-warning" >Add student
				</button></a>
</body>
</html>